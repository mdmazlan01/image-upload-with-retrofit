package com.bugfighters.imageuploadwithretrofit;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private ImageView imageView;
    private Button button;

    public  Uri uri;

    private final int REQUEST_GALLARY=10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestPermissions();

        setContentView(R.layout.activity_main);

        imageView=findViewById(R.id.image_id);
        button=findViewById(R.id.upload_id);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
             //   intent.setType("image/*");

                startActivityForResult(Intent.createChooser(intent,"Select A PPicture"),REQUEST_GALLARY);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uploadFile();


            }
        });

    }

    private void requestPermissions() {
        if (Build.VERSION.SDK_INT>=23){

            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},1);

                Toast.makeText(getApplicationContext(), "Please Allow Permission", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode==RESULT_OK){
            if (requestCode==REQUEST_GALLARY) {
                Log.d("image","onActivityResult:"+data.getData());
                Uri uri = data.getData();
                Glide.with(this).load(uri).into(imageView);

            }
        }
    }

    private void uploadFile() {

        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();
        final Bitmap bitmapname = Bitmap.createBitmap(imageView.getDrawingCache());
        String image=imagetoString(bitmapname);
      //  Log.d("image","upload:"+uri.toString()); for url find
        Log.d("image","upload:"+image); //for image check

        ApiServices apiServices =ApiClient.getApiClient().create(ApiServices.class);

        apiServices.test_name("Taka",image).enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {

                Log.d("res","Hello"+response.body().getSuccess());

            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {
                Log.d("res","Hello"+t.toString());
            }
        });

    }

    private String imagetoString(Bitmap bitmapname) {

        ByteArrayOutputStream bts = new ByteArrayOutputStream();
        bitmapname.compress(Bitmap.CompressFormat.PNG,100,bts);
        byte [] bytes = bts.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
        }
}