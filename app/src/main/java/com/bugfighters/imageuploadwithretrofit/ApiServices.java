package com.bugfighters.imageuploadwithretrofit;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiServices {

    @FormUrlEncoded
    @POST("image.php")
    Call<Model> test_name(@Field("image_name") String Name,@Field("image") String image );


}


